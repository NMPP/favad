#!/bin/bash
############################################################################################################### 
## Copyright (C) 2020 Udo von Toussaint, F. J. Dominguez-Gutierrez, Markus Rampp, Michele Compostella 
##              Max-Planck-Institut für Plasmaphysik, Boltzmannstrasse 2, 85748 Garching, Germany 
##              Max-Planck Computing and Data Facility, Giessenbachstrasse 2, 85748 Garching, Germany 
## 
## This program is free software; you can redistribute it and/or modify 
## it under the terms of the GNU General Public License as published by 
## the Free Software Foundation; either version 2, or (at your option) 
## any later version. 
## 
## This program is distributed in the hope that it will be useful, 
## but WITHOUT ANY WARRANTY; without even the implied warranty of 
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
## GNU General Public License for more details. 
## 
## You should have received a copy of the GNU General Public License 
## along with this program; if not, write to the Free Software 
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
## 02110-1301, USA. 
## 
#########################################################################
# This installer works on Ubuntu 18.04 LTS
#
# It installs all the requirements for running the fingerprinting 
# and visualization of defects in damages crystal structures
#
# Run with (requires root permissions):
# ./installer.sh /installation_path/ full_name email_address organization
#
# For example:
# ./installer.sh /home/aurelius/software/ "Marcus Aurelius" marcus.aurelius@email.com "Imperium Romanum"
#
# Note that "full_name", "email_address" and "organization" are required
# to accept the GAP suite license.
#
#########################################################################

# echo an error message if any command fails 
set -e

# Updating distro and installing packages
echo "Updating LINUX distribution..."
sudo apt update

echo "Installing LINUX packages..."
sudo apt install gcc gfortran libblas-dev liblapack-dev python python-pip python3 python3-pip ffmpeg imagemagick libsm6 libgl1 libgl1-mesa-dri libgl1-mesa-glx libpcre3-dev libxt6

# Storing repository location
REPO_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

# Creating software folder
mkdir -p $1

pushd $1

  # Installing Python 3 packages
  echo "Installing Python 3 packages..."
  pip3 install --user numpy
  pip3 install --user matplotlib
  pip3 install --user vtk
  pip3 install --user mayavi

  # Installing QUIP with GAP
  echo "Installing QUIP with GAP..."
  git clone --recursive https://github.com/libAtoms/QUIP.git
  cd QUIP
  curl -X POST -F "name=$2" -F "email=$3" -F "org=$4" -F "agree=I accept" --output GAP.tar.gz  http://www.libatoms.org/gap/cgi-bin/script.cgi
  tar xvzf ./GAP.tar.gz -C $1/QUIP/src/.
  # Installing
  export QUIP_ARCH=linux_x86_64_gfortran
  (echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo y ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ) | make config
  make
  export QUIP_STRUCTS_DIR=$1/QUIP/structs
  export QUIP_INSTALLDIR=$1/QUIP/bin
  make install
  cd ..

  # Installing voro++
  wget http://math.lbl.gov/voro++/download/dir/voro++-0.4.6.tar.gz
  mkdir -p $1/voro++
  tar xvzf ./voro++-0.4.6.tar.gz -C $1/voro++ --strip-components 1
  cd voro++/
  sed -i "21s|.*|PREFIX=${1}/voro++|" ./config.mk
  make
  make install
  cd ..

  # Installing KDTREE
  echo "Installing KDTREE..."
  KDTREE_PATH=$REPO_PATH/../software/
  tar xvzf $KDTREE_PATH/kdtree2.tar.gz -C $1/.
  cd kdtree2/
  gfortran -c kdtree2.f90
  gfortran -o kdtree2_iaea_v03.x kdtree2_iaea_v03.f90 kdtree2.o
  cd ..

  # Installing VisIt
  echo "Installing VISIT..."
  wget https://portal.nersc.gov/project/visit/releases/2.13.3/visit2_13_3.linux-x86_64-ubuntu18-wmesa.tar.gz
  wget https://portal.nersc.gov/project/visit/releases/2.13.3/visit-install2_13_3
  chmod 755 ./visit-install2_13_3
  mkdir -p $1/visit/
  (echo 1) | bash ./visit-install2_13_3 2.13.3 linux-x86_64-ubuntu18-wmesa $1/visit/
  
  # Archiving not required files
  mkdir archive/
  mv ./QUIP/GAP.tar.gz archive/
  mv ./voro++-0.4.6.tar.gz archive/
  cp $KDTREE_PATH/kdtree2.tar.gz archive/
  mv ./visit2_13_3.linux-x86_64-ubuntu18-wmesa.tar.gz archive/
  mv ./visit-install2_13_3 archive/

popd

echo
echo "Local installation completed!"
echo "A copy of the installed packages is available in $1/archive"
echo

exit 0

