# A software workflow for fingerprinting and visualizing defects in damaged crystal structures
---
#### Udo von Toussaint, F. J. Domı́nguez-Gutiérrez, Markus Rampp, Michele Compostella
###### Max-Planck-Institut für Plasmaphysik, Boltzmannstrasse 2, 85748 Garching, Germany
###### Max-Planck Computing and Data Facility, Giessenbachstrasse 2, 85748 Garching, Germany
---


## Introduction

This repository contains the recipe to create a Docker container where users can test the workflow for fingerprinting and visualizing defects in damaged crystal structures.

Together with the required software, data for the analysis of point defects in a bcc Fe sample are provided. This dataset was obtained by performing MD simulations to emulate neutron bombardment at a primary-knock on-atom of 10 keV.


## Requirements

In order to build the Docker image, the following software is required on the local machine:  

* tar
* curl
* wget
* git
* [docker](https://docs.docker.com/install/)

#### Important:
The workflow presented in this repository uses the GAP library for the software QUIP. When installing this software, you are accepting the [GAP license agreement](http://www.libatoms.org/gap/gap_download.html).


## Installation

Clone this repository to your local machine with
```shell
git clone https://gitlab.mpcdf.mpg.de/NMPP/favad.git
```

After cloning this repository, run the following commands (note that building the Docker image requires root permission on the local machine) to create the Docker image  

```shell
cd favad/
./build.sh
```

The entire build process may take several minutes.  
The build.sh script accepts additional optional parameters in order to automatically accept the [GAP license agreement](http://www.libatoms.org/gap/gap_download.html):

```shell
./build.sh "I accept" "FULL NAME" "EMAIL ADDRESS" "ORGANIZATION"
```
If these optional parameters are not specified, the build process will ask for them during runtime.


## How to run the Docker image

Once the Docker image has been created and the [Docker daemon is running](https://docs.docker.com/config/daemon/), you can create a local folder where the output files will be stored using, for example:

```shell
mkdir $HOME/favad_output/
```

Finally, shell into the Docker container mounting the created folder:

```shell
sudo docker run -t -i --volume $HOME/favad_output/:/home/favad/sample/output/ favad /bin/bash
```

Using the previous command, the ouput files generated inside the Docker container will be available in $HOME/favad_output/ of your local machine. Please note that the content of this folder will be overwritten, if the folder is not empty.

At this point you can run the workflow for fingerprinting and visualizing defects on the sample dataset inside the Docker container:
```shell
cd /home/favad/sample/
python3 FaVAD.py -p /home/favad/sample/parameters.txt
```

When the rendering is completed, you can log out the Docker container using

```shell
exit
```

and examine the output in the $HOME/favad_output/ folder.  
For a complete list of Docker commands and options, please check the [Docker documentation](https://docs.docker.com/) and the list of [Docker base commands](https://docs.docker.com/engine/reference/commandline/docker/).  

By default, only a single image (and its components in the different colour channels) for the input dataset is generated.
In order to generate a full movie, please set 'MOVIE_CREATION = True' in the parameters.txt file before running the FaVaD.py script.  

Note that running the Docker container requires root priviledges on the machine. Since, for security reason, Docker is usually not available on computer clusters, it is advisable to use online tools to convert the Docker container into other containerized environment (e.g. Singularity, Linux containers, etc.) in order to run on HPC systems.

---

## Local Installation

In the 'assets' folder we provide a script for the installation of the entire software stack on a local machine (instead of inside a Docker container) running Ubuntu 18.04 LTS.  
In this case, you can run the installer using
```shell
./assets/installer.sh /local_installation_path/ "FULL NAME" "EMAIL ADDRESS" "ORGANIZATION"
```
By running the installer, you are accepting the [GAP license agreement](http://www.libatoms.org/gap/gap_download.html).


## How to run locally

If you installed the software stack locally, you can run the workflow for fingerprinting and visualizing defects on the sample dataset using
```shell
cd ./sample
python3 FaVAD.py -p ./parameters.txt
```
Note however that you are required to change the paths in the parameter.txt file to match the current location of the files and software executables.

---

## Analyzing new crystal structures

Once the software run on the Fe sample dataset, the next step is to use the pipeline presented here for fingerprinting and visualizing defects in other crystal structures.  
In order to do this, the following changes to the pipeline are required:
* An input file for the new structure should be created (e.g. using MD simulations) and stored in the xyz format.  
* Files containing the reference descriptor vectors for the undamaged crystal should be provided. In the sample dataset, we supply for this purpose the following four files:  
  &nbsp; &nbsp; bcc_vector.dat  
  &nbsp; &nbsp; interstitial_vector.dat  
  &nbsp; &nbsp; typea_vector.dat  
  &nbsp; &nbsp; vacancy_vector.dat    
* A text file containing the parameters for the KDTREE code is required (void_code_input.txt in the sample dataset) in order to choose the correct settings for the code.  
* The parameter file named parameters.txt for the FaVaD.py script should be updated with the new locations and names of all the input files. A short description of every keyword is available as comment in the parameter file itself.  
* [OPTIONAL] The visualization script for VisIt (vis.py) should be updated with new values for the threshold that controls the number of plotted atoms.  
   

In order to make the new input files available inside the Docker container, the flag 
```shell
--volume $HOME/new_inputs/:/home/favad/new_inputs/:ro
```
should be added to the 'docker run' command above. This will mount in read-only mode the '$HOME/new_inputs/' folder and its content from the local machine to the Docker container at the '/home/favad/new_inputs/' location.

In the folder 'sample/input/descriptor_vectors' we provide the python scripts (and the associated output files) used to calculate the descript vectors of different crystal structures.
These files provide a starting point for the analysis of new crystal structures obtained from MD simulations.
