#!/bin/bash
###############################################################################################################
## Copyright (C) 2020 Udo von Toussaint, F. J. Dominguez-Gutierrez, Markus Rampp, Michele Compostella
##              Max-Planck-Institut für Plasmaphysik, Boltzmannstrasse 2, 85748 Garching, Germany
##              Max-Planck Computing and Data Facility, Giessenbachstrasse 2, 85748 Garching, Germany
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
###############################################################################################################
##
## General Notes:
## Docker container builder for Fingerprinting and Visualizing defects in damaged crystal structures
## Version: 1.0
## This software provides a workflow for fingerprinting and visualizing defects in damaged crystal 
## structures. The theoretical background is presented in: paper_ID
##
## Run with:
## ./build.sh "I agree" full_name email_address organization
##
## For example:
## ./build.sh "I agree" "Marcus Aurelius" marcus.aurelius@email.com "Imperium Romanum"
##
###############################################################################################################

# echo an error message if any command fails
set -e
set -o pipefail
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'echo "\"${last_command}\" command failed with exit code $?."; echo; echo "Build process interrupted!"; echo' EXIT

# Reading input parameters
GAP=$1
NAME=$2
EMAIL=$3
ORGANIZATION=$4

# GAP license
if [ ! -f ./software/GAP.tar.gz ]; then
   if [ "$GAP" != "I accept" ]; then
      echo
      echo "You need to accept the GAP license to create this Docker container"
      echo "The GAP suite — Non-commercial License Agreement is available at:"
      echo "  http://www.libatoms.org/gap/gap_download.html"
      echo
      echo "Read the Agreement at the link in full."
      echo "Only if you accept the Agreement, type 'I accept' followed by [ENTER]."
      read GAP
      if [ "$GAP" != "I accept" ]; then
         echo "You did not accept the GAP license. Leaving installation process.."
         trap - EXIT
         exit
      fi
      echo "Enter your name for the GAP license"
      read NAME
      echo "Enter your email address for the GAP license"
      read EMAIL
      echo "Enter your organization name for the GAP license"
      read ORGANIZATION
   fi

   # Downloading GAP in ./software/
   echo
   echo "Downloading GAP.."
   curl -X POST -F "name=$NAME" -F "email=$EMAIL" -F "org=$ORGANIZATION" -F "agree=$GAP" --output ./software/GAP.tar.gz  http://www.libatoms.org/gap/cgi-bin/script.cgi
fi

# Downloading required software
if [ ! -f ./software/kdtree2.tar.gz ]; then
   echo
   echo "kdtree2.tar.gz archive is missing from the ./software/ folder. Leaving installation.."
   trap - EXIT
   exit
fi

if [ ! -f ./software/voro++-0.4.6.tar.gz ]; then
   echo
   echo "Downloading VORO++.."
   wget -P ./software/ http://math.lbl.gov/voro++/download/dir/voro++-0.4.6.tar.gz
fi

if [ ! -f ./software/QUIP.tar.gz ]; then
   echo
   echo "Downloading QUIP.."
   git clone --recursive https://github.com/libAtoms/QUIP.git ./QUIP/
   tar -czvf ./software/QUIP.tar.gz ./QUIP/*
   rm -rf ./QUIP/
fi

if [ ! -f ./software/visit-install2_13_3 ]; then
   echo
   echo "Downloading VisIt.."
   wget -P ./software/ https://portal.nersc.gov/project/visit/releases/2.13.3/visit-install2_13_3
   wget -P ./software/ https://portal.nersc.gov/project/visit/releases/2.13.3/visit2_13_3.linux-x86_64-ubuntu18-wmesa.tar.gz
fi

# Build the Docker container "favad" from the definition file 'Dockerfile' in the current directory
sudo docker build --tag favad:v1.0 -t favad .

echo
echo
echo
echo "Build complete!"
echo
echo "You can run the image interactively mounting an ouput folder (use the full path) with:"
echo "  sudo docker run -t -i --volume $HOME/favad_output/:/home/favad/sample/output/ favad /bin/bash"
echo "and, once in the container" 
echo "  cd /home/favad/sample/"
echo "  python3 FaVAD.py -p /home/favad/sample/parameters.txt"
echo

# Disarming trap and exit
trap - EXIT
exit 0

