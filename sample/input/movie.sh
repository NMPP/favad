#!/bin/bash

# Converting R, G, B channels into single RGB image
for file in $1/*_R.png
do
    fileR=$file
    fileG=`echo $file | sed 's/_R/_G/'`
    fileB=`echo $file | sed 's/_R/_B/'`
    fileV=`echo $file | sed 's/_R/_V/'`

    if [ -e $fileR ] && [ -e $fileG ] && [ -e $fileB ] && [ -e $fileV ] 
    then
        fileVR=`echo $file | sed 's/_R/_VR/'`
        fileVRG=`echo $file | sed 's/_R/_VRG/'`
        fileVRGB=`echo $file | sed 's/_R/_VRGB/'`
        (composite -compose add $fileV $fileR $fileVR && composite -compose add $fileVR $fileG $fileVRG && composite -compose add $fileVRG $fileB $fileVRGB) && rm -f $fileVR $fileVRG #&& rm -f $fileV $fileR $fileG $fileB $fileVR $fileVRG
        echo "... done $fileVRGB"
    else
        echo "skipping $fileR $fileG $fileB $fileV"
    fi
done

# Creating folders for the movie
mkdir $1/frames
mkdir $1/frames_merged
mkdir $1/movie
mkdir $1/movie/1-intro-1f/
mkdir $1/movie/2-fading-45f/
mkdir $1/movie/3-still-1f/
mkdir $1/movie/4-rotation-360f/
mkdir $1/movie/5-still-1f

# Copying frames
cp $1/*_VRGB.png $1/frames_merged/.
mv $1/*_VRGB.png $1/movie/.
mv $1/*.png $1/frames/.

cd $1/movie
cp $2-0000_VRGB.png 1-intro-1f/
cp $2-0045_VRGB.png 3-still-1f/
cp $2-0045_VRGB.png 4-rotation-360f/$2-0405_VRGB.png
cp $2-0045_VRGB.png 5-still-1f/

for file in $2-000{0..9}_VRGB.png; do mv $file $1/movie/2-fading-45f; done
for file in $2-00{10..44}_VRGB.png; do mv $file $1/movie/2-fading-45f; done
mv *.png $1/movie/4-rotation-360f/

# Creating movie
WORKDIR=$1/movie/

cd $WORKDIR/1-intro-1f/
ffmpeg -loop 1 -i $2-0000_VRGB.png -c:v libx264 -t 1 -pix_fmt yuv420p intro.mp4

cd $WORKDIR/2-fading-45f/
ffmpeg -framerate 20 -start_number 0 -i $2-%04d_VRGB.png -c:v libx264 -vf "fps=30,format=yuv420p" fading.mp4

cd $WORKDIR/3-still-1f/
ffmpeg -loop 1 -i $2-0045_VRGB.png -c:v libx264 -t 1 -pix_fmt yuv420p still.mp4

cd $WORKDIR/4-rotation-360f/
ffmpeg -framerate 15 -start_number 45 -i $2-%04d_VRGB.png -c:v libx264 -vf "fps=30,format=yuv420p" rotation.mp4

cd $WORKDIR/5-still-1f/
ffmpeg -loop 1 -i $2-0045_VRGB.png -c:v libx264 -t 8 -pix_fmt yuv420p still.mp4

cd $WORKDIR/
cat > list.txt <<EOF
file '$WORKDIR/1-intro-1f/intro.mp4'
file '$WORKDIR/2-fading-45f/fading.mp4'
file '$WORKDIR/3-still-1f/still.mp4'
file '$WORKDIR/4-rotation-360f/rotation.mp4'
file '$WORKDIR/5-still-1f/still.mp4'
EOF

ffmpeg -f concat -safe 0 -i list.txt -c copy $2.mp4

# Removing temporary files used for the creation of the movie
rm -rf $WORKDIR/1-intro-1f/
rm -rf $WORKDIR/2-fading-45f/
rm -rf $WORKDIR/3-still-1f/
rm -rf $WORKDIR/4-rotation-360f/
rm -rf $WORKDIR/5-still-1f/
rm -rf $WORKDIR/list.txt

echo 'Movie Creation Terminated'
exit 0

