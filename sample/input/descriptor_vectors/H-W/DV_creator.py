####################################################################################################
## Copyright (C) 2020 Udo von Toussaint, F. J. Dominguez-Gutierrez, Michele Compostella, Markus Rampp
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
#####################################################################################################
##
## General Notes:
## FaVAD: Fingerprinting And Visualization Analyzer of Defects. Version: 1.0
## Python script to generate the set of standard descriptor vectors
## Please modify the code to create the chosen reference descriptor vector of the standard defect
## The geometry of the standard defect is defined in a xyz file
##
#####################################################################################################

import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import linecache
import subprocess
import sys
#########################################################
num_lines = sum(1 for line in open('H_W_geometry.xyz'))
num_atom = int(linecache.getline('H_W_geometry.xyz',1))
print('checking xyz file')
num_lines = num_lines-2
if ( num_lines== num_atom): 
   print('xyz is in good shape')
else:
   sys.exit("xyz file corrupted, please check its format")
##### computing descriptor vectors of damaged sample ####
print('computing standard descriptor vector')
# command line for hundreds to thousands of atoms
cmd = 'quip atoms_filename=H_W_geometry.xyz descriptor_str="soap cutoff=3.0 l_max=4 n_max=4 atom_sigma=0.5 n_Z=2 Z={74 1} n_species=2 species_Z={74 1}" > vectors.dat'
#
subprocess.call(cmd,shell=True)

i = 0
arr = []
# Cleaning quip output file
# Start reading DV at line 11
for i in range(11,11+num_atom):
     dv_1 = np.array(linecache.getline('vectors.dat',i)[7::].split())
     dv = dv_1.astype(np.float)
     arr.append(dv)

arr = np.array(arr)
DV_length = len(arr[0,:])
print('The number of DV components is: {0}'.format(DV_length))
#### Printing out results
#### bcc DV ##################################
print('Results are given in: bcc_W_vector.dat')
file_1 = open('bcc_W_vector.dat','w')
for i in range(0,DV_length):
    file_1.write("{0} \n".format(arr[0,i]))


#### interstitial DV ##################################
print('Results are given in: interstitial_H_vector.dat')
file_1 = open('interstitial_H_vector.dat','w')
for i in range(0,DV_length):
    file_1.write("{0} \n".format(arr[432,i]))

#### W(first NN) DV ##################################
print('Results are given in: W_first_NN_vector.dat')
file_1 = open('W_first_NN_vector.dat','w')
for i in range(0,DV_length):
    file_1.write("{0} \n".format(arr[54,i]))

#### W(second NN) DV ##################################
print('Results are given in: W_second_NN_vector.dat')
file_1 = open('W_second_NN_vector.dat','w')
for i in range(0,DV_length):
    file_1.write("{0} \n".format(arr[361,i]))


#cleaning files
print('cleaning...')
cmd1 = 'rm vectors.dat'
subprocess.call(cmd1,shell=True)
print('Done!')
