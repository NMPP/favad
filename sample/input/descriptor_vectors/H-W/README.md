# DV computation for multi component materials

The geometry of a hydrogen atom in an interstitial site in a W sample was 
obtained as follows:
1.  A pristine W sample based in a bcc unit cell is build and energy 
    optimized by using lammps with the tersoff potentials.
2.  Then a H atom was inserted in a tetrahedral site into the 
    optimized W sample.
3. The sample with a H atoms is energy optimized with lammps and 
    tersoff potentials.
4. The obtained W-H sample is used to compute the DVs of:

   *  bcc W atom
   *  H atom in a tetrahedral instersitial site
   *  W atom as first nearest neighbor to the H atom
   *  W atom as second nearest neighbor to the H atom

Attention need to be paid to the quip command line to compute the 
DVs for the hydrogenated W sample
The command line is:
     
     quip atoms_filename=H_W_geometry.xyz descriptor_str="soap cutoff=3.0 l_max=4 n_max=4 atom_sigma=0.5 n_Z=2 Z={74 1} n_species=2 species_Z={74 1}"

where:

    n_Z=2 is defined for 2 species in the material sample, this value can be changed
          for material with more than 2 species.
    Z={74 1} this parameter is defined according to the charge of the species, here 
             we consider that W = 74 and H = 1. The order of these number needs to be
             defined according to the list of atoms defined in the xyz file. 
    n_species=2 this is the number of species in the material
    species_Z={74 1}  the order for this parameter is defined by the xyz file.
    
Details can be found here: 
F. J. Dominguez-Gutierrez and U. von Toussaint.
On the detection and classification of material defects in crystalline solids after energetic particle impact simulations
Journal of Nuclear Materials 528, 151833 (2020).
https://doi.org/10.1016/j.jnucmat.2019.151833