#####################################################################################################
## Copyright (C) 2020 Udo von Toussaint, F. J. Dominguez-Gutierrez, Michele Compostella, Markus Rampp
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
##################################################################################
## General Notes:
## VisIt script for the visualization of defects in damaged crystal structures
##
## Last update: 15.04.2020
## Tested with VisIt 2.13.3
## at: Max Planck Computing and Data Facility, Garching
##
## run with:
## visit -cli -nowin -s vis.py
##
##################################################################################

# Parameters of the script
input_s = '/home/favad/sample/output/fe_sample.DVs.xyz' 
input_v = '/home/favad/sample/output/near_d.vtk' 
out_dir = '/home/favad/sample/output/visualization/' 
out_name = 'fe' 

fading = False
n_frames = 1

# Modules
import math

# Definitions
rgb0 = {"R":(100, 0, 0, 85), "G":(0, 100, 0, 85), "B":(0, 0, 0, 85)}
rgb1 = {"R":(255, 0, 0, 85), "G":(0, 255, 0, 85), "B":(0, 0, 255, 85)}

rgbgrey  = {"R":(50, 0, 0, 255), "G":(0, 50, 0, 255), "B":(0, 0, 50, 255)}
rgbwhite = {"R":(255, 0, 0, 255), "G":(0, 255, 0, 255), "B":(0, 0, 255, 255)}

colmap = "Purples"   # color map for the volume plot
#for ctnames in ColorTableNames():
#   print "- "+ctnames

x = {"R":"m_typea","G":"voidint","B":"m_void"}

#create R,G,B color tables
for ch in ["R","G","B"]:
    ccpl = ColorControlPointList()
    ccpl.AddControlPoints(ColorControlPoint())
    ccpl.AddControlPoints(ColorControlPoint())
    ccpl.GetControlPoints(0).colors = rgb0[ch]
    ccpl.GetControlPoints(0).position = 0
    ccpl.GetControlPoints(1).colors = rgb1[ch]
    ccpl.GetControlPoints(1).position = 1.0
    AddColorTable(ch, ccpl)

#create a black color table
    ccpl = ColorControlPointList()
    ccpl.AddControlPoints(ColorControlPoint())
    ccpl.AddControlPoints(ColorControlPoint())
    ccpl.GetControlPoints(0).colors = (0, 0, 0, 255)
    ccpl.GetControlPoints(0).position = 0
    ccpl.GetControlPoints(1).colors = (0, 0, 0, 255)
    ccpl.GetControlPoints(1).position = 1.0
    AddColorTable("black", ccpl)

# Plotting Variables
DefineScalarExpression("m_typea","-var3")
DefineScalarExpression("m_void","exp(-var2)")
DefineScalarExpression("voidint","var2-(1.0-var1)")

# Scaling atom size according to the cubic root of variable var4
DefineScalarExpression("av_rad","(var4)^(1./3.)")


def addbox():
    AddOperator("Box",0)
    b = BoxAttributes()
    b.amount = b.All
    b.minx = -60.
    b.maxx = 60.
    b.miny = -80.
    b.maxy = 40.
    b.minz = -60.
    b.maxz = 60.
    SetOperatorOptions(b)


def addthreshold():
    AddOperator("Threshold",0)
    t = ThresholdAttributes()
    t.listedVarNames = ("var0")
    t.lowerBounds = (0.1)
    #t.upperBounds = (max)
    SetOperatorOptions(t)


def setview(counter,tot):
   #Setting the fading view (should match the first frame of the loop on angles)
   phi0 = 0.
   phi = phi0+2*math.pi*(float(counter)/float(tot))
   c = View3DAttributes()
   c.viewNormal = (math.sin(phi),0.,math.cos(phi))
   c.focus = (0, 0, 0)
   c.viewUp = (0, 1, 0)
   c.viewAngle = 30
   c.parallelScale = 120.
   c.nearPlane = -240.0
   c.farPlane = 240.0
   c.imagePan = (-0.002, 0.1)
   c.imageZoom = 1.5
   c.perspective = 1
   c.eyeAngle = 2
   c.centerOfRotationSet = 1
   c.centerOfRotation = (0., -20., 0.)
   c.shear = (0, 0, 1)
   SetView3D(c)


# Annotations and properties
AnnotationAtts = AnnotationAttributes()
AnnotationAtts.axes3D.visible = 0
AnnotationAtts.axes3D.lineWidth = 2
AnnotationAtts.axes3D.triadFlag = 0
AnnotationAtts.legendInfoFlag = 0
AnnotationAtts.axes3D.bboxFlag = 1
AnnotationAtts.userInfoFlag = 0
AnnotationAtts.databaseInfoFlag = 0
AnnotationAtts.backgroundColor = (0, 0, 0, 255)
AnnotationAtts.foregroundColor = (255, 255, 255, 255)
AnnotationAtts.axes3D.setBBoxLocation = 1
AnnotationAtts.axes3D.bboxLocation = (-60., 60., -76., 40., -60., 60.)
SetAnnotationAttributes(AnnotationAtts)

# Save options
s = SaveWindowAttributes()
s.format = s.PNG
s.family = 0
s.resConstraint = 0
s.screenCapture = 0
s.outputToCurrentDirectory = 0
s.outputDirectory = out_dir
s.width, s.height = 1024,1024
SetSaveWindowAttributes(s)

# Plotting the volume
OpenDatabase(input_v, 0)
AddPlot("Volume", "near_d")
vol = VolumeAttributes()
vol.SetColorControlPoints(GetColorTable(colmap))
vol.colorControlPoints.smoothing = vol.colorControlPoints.Linear  # None, Linear, CubicSpline
vol.opacityAttenuation = 1
vol.opacityMode = vol.FreeformMode  # FreeformMode, GaussianMode, ColorTableMode
vol.resampleTarget = 1000000
vol.opacityVariable = "default"
vol.compactVariable = "default"
vol.freeformOpacity = (255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255)
vol.useColorVarMin = 0
vol.colorVarMin = 3.1
vol.useColorVarMax = 0
vol.colorVarMax = 0
vol.useOpacityVarMin = 0
vol.opacityVarMin = 0
vol.useOpacityVarMax = 0
vol.opacityVarMax = 0
vol.rendererType = vol.Splatting
SetPlotOptions(vol)

AddOperator("Threshold",0)
t = ThresholdAttributes()
t.lowerBounds = (3.2)
#t.upperBounds = (max)
SetOperatorOptions(t)

addbox()

# Plotting the atoms
OpenDatabase(input_s, 0)
AddPlot("Pseudocolor",x["R"])
p = PseudocolorAttributes()
p.colorTableName = "R"
p.pointType = p.SphereGeometry
p.pointSize = 1.
p.pointSizeVarEnabled = 1
p.pointSizeVar = "av_rad"
SetPlotOptions(p)

addthreshold()

addbox()

# Producing the plots 
#DrawPlots()


# Fading option
if fading:
   # Adding the fade-out atoms
   AddPlot("Pseudocolor",x["R"])
   p = PseudocolorAttributes()
   p.colorTableName = "R"
   p.pointType = p.SphereGeometry
   p.pointSize = 1.
   p.pointSizeVarEnabled = 1
   p.pointSizeVar = "av_rad"
   SetPlotOptions(p)
   
   addbox()
   
   #loop over transition
   fadesteps=45
   for i in range(0,fadesteps):
      ###
      # Step1: colorized volume, black atoms
      SetActivePlots(0)
      vol = GetPlotOptions()
      vol.SetColorControlPoints(GetColorTable(colmap))
      SetPlotOptions(vol)
      
      SetActivePlots(1)
      p = GetPlotOptions()
      p.colorTableName = "black"
      SetPlotOptions(p)
      
      SetActivePlots(2)
      p = GetPlotOptions()
      p.colorTableName = "black"
      p.pointSize = 1.*(1.-float(i)/float(fadesteps))
      SetPlotOptions(p)

      # Save image
      #RedrawWindow()
      ClearWindow()
      DrawPlots()
      setview(0,1)
      s = GetSaveWindowAttributes()
      s.fileName = out_name+"-"+"%04d" % (i) +"_V"
      SetSaveWindowAttributes(s)
      SaveWindow()

      ###
      # Step2: black volume, colorized atoms in 3 channels
      SetActivePlots(0)
      vol = GetPlotOptions()
      vol.SetColorControlPoints(GetColorTable("black"))
      SetPlotOptions(vol)

      for ch in ["R","G","B"]:
         SetActivePlots(1)
         ChangeActivePlotsVar(x[ch])
         p = GetPlotOptions()
         p.colorTableName = ch
         SetPlotOptions(p)

         SetActivePlots(2)
         ChangeActivePlotsVar(x[ch])
         p = GetPlotOptions()
         p.colorTableName = ch
         p.pointSize = 1.*(1.-float(i)/float(fadesteps))
         SetPlotOptions(p)

         # Save image
         #RedrawWindow()
         ClearWindow()
         DrawPlots()
         setview(0,1)
         s = GetSaveWindowAttributes()
         s.fileName = out_name+"-"+"%04d" % (i) +"_"+ch
         SetSaveWindowAttributes(s)
         SaveWindow()

   SetActivePlots(2)
   DeleteActivePlots()
else:
   fadesteps=0


#loop over angles
nsteps= n_frames   # 1 to 360
for i in range(0,nsteps):
   ###
   # Step1: colorized volume, black atoms
   SetActivePlots(0)
   vol = GetPlotOptions()
   vol.SetColorControlPoints(GetColorTable(colmap))
   SetPlotOptions(vol)

   SetActivePlots(1)
   p = GetPlotOptions()
   p.colorTableName = "black"
   SetPlotOptions(p)
   
   # Save image
   #RedrawWindow()
   ClearWindow()
   DrawPlots()
   setview(i,nsteps)
   s = GetSaveWindowAttributes()
   s.fileName = out_name+"-"+"%04d" % (i + fadesteps) +"_V"
   SetSaveWindowAttributes(s)
   SaveWindow()

   ###
   # Step2: black volume, colorized atoms in 3 channels
   SetActivePlots(0)
   vol = GetPlotOptions()
   vol.SetColorControlPoints(GetColorTable("black"))
   SetPlotOptions(vol)
   
   SetActivePlots(1)
   for ch in ["R","G","B"]:
      ChangeActivePlotsVar(x[ch])
      p = GetPlotOptions()
      p.colorTableName = ch
      SetPlotOptions(p)
      
      # Save image
      #RedrawWindow()
      ClearWindow()
      DrawPlots()
      setview(i,nsteps)
      s = GetSaveWindowAttributes()
      s.fileName = out_name+"-"+"%04d" % (i + fadesteps) +"_"+ch
      SetSaveWindowAttributes(s)
      SaveWindow()

    # Closing VisIt
quit()


