#####################################################################################################
## Copyright (C) 2020 Udo von Toussaint, F. J. Dominguez-Gutierrez, Michele Compostella, Markus Rampp
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
#####################################################################################################
##
## General Notes:
## FaVAD: Fingerprinting And Visualization Analyzer of Defects. Version: 1.0
## This software provides a workflow for fingerprinting and visualizing defects in damaged crystal 
## structures. The theoretical background is presented in: paper_ID
##
#####################################################################################################


##########
# Preamble
##########

import sys, argparse
import os, subprocess
import gc

os.system('clear')
print()
print(" ------------------------------------------------- ")
print("|     Fingerprint and visualization of defects    |")
print("|          in damaged crystal structures          |")
print(" ------------------------------------------------- ")
print()

# Check Python version
if sys.version_info[0] < 3:
    print("\nRequires Python version 3 or greater\n")
    sys.exit()

# This is used in order to read negative numbers, without seeing them as other parameters
for i, arg in enumerate(sys.argv):
     if (arg[0] == '-') and arg[1].isdigit(): sys.argv[i] = ' ' + arg
    
# Reading and setting the different arguments
parser = argparse.ArgumentParser(description='This is a Python 3 script for fingerprinting and visualizing defects in damaged crystal structures.')
parser.add_argument('-p','--parameters', help="Path to the file with the parameters",required=True)
args = parser.parse_args()

# Cheching if input parameter file exists
if (not os.path.exists(args.parameters)):
    print("Parameter file does not exist.")
    quit()

# Creating dictionary with entries from the parameter file
param = {}
with open(args.parameters) as f:
    lines = filter(None, (line.rstrip() for line in f))
    for line in lines:
        if line.startswith("#"):
            continue          
        (key, temp ,val) = line.split()
        param[key] = val

# Adding / to output folder, if missing
if (param['OUTPUT_PATH'][-1]!='/'):
    param['OUTPUT_PATH']=param['OUTPUT_PATH']+'/'

# Checking for output folder
try:
    os.makedirs(param['OUTPUT_PATH'])
except FileExistsError:
    # directory already exists
    print("\nWarning: output directory exists. Files will be overwritten.")
    input("         Press Enter to continue.")
    print()

# Cheching if input files exist
if (not os.path.exists(param['QUIP_XYZ'])):
    print("QUIP xyz input file does not exist.")
    quit()
if (not os.path.exists(param['BCC_VEC'])):
    print("bcc reference descriptor vector file does not exist.")
    quit()
if (not os.path.exists(param['INT_VEC'])):
    print("Interstitial reference descriptor vector file does not exist.")
    quit()
if (not os.path.exists(param['VAC_VEC'])):
    print("Next-to-vacancy reference descriptor vector file does not exist.")
    quit()
if (not os.path.exists(param['TYPA_VEC'])):
    print("Type-a reference descriptor vector file does not exist.")
    quit()

# Cheching if commands exist
if (not os.path.exists(param['QUIP_PATH'])):
    print("Path to QUIP executable does not exist.")
    quit()
if (not os.path.exists(param['KDTREE_PATH'])):
    print("Path to KDTREE executable does not exist.")
    quit()
if (not os.path.exists(param['VISIT_PATH'])):
    print("Path to VISIT executable does not exist.")
    quit()


##############################
# Calculation of DVs with QUIP
##############################

import numpy as np
from numpy import *
import matplotlib.pyplot as plt
import linecache

num_lines = sum(1 for line in open(param['QUIP_XYZ']))
num_atom = int(linecache.getline(param['QUIP_XYZ'],1))
print('Checking xyz input file')
num_lines = num_lines-2
if ( num_lines == num_atom):
   print('Input xyz is in good shape')
else:
   sys.exit("Input xyz file corrupted, please check its format")
print()

# Computing descriptor vectors of damaged sample
print('Computing descriptor vectors of damaged material with QUIP')
# command line for hundreds to thousands of atoms
cmd = str(param['QUIP_PATH'])+' atoms_filename='+str(param['QUIP_XYZ'])+' descriptor_str="soap cutoff=2.75 l_max=4 n_max=4 atom_sigma=0.5 n_Z=1 Z={26} n_species=1 species_Z={26}" > '+str(param['OUTPUT_PATH'])+'vectors.dat'
# command line for million of atoms
#cmd = 'mpirun -n 24 '+str(param['QUIP_PATH'])+'_openmpi atoms_filename='+str(param['QUIP_XYZ'])+' descriptor_str="soap cutoff=2.75 l_max=4 n_max=4 atom_sigma=0.5 n_Z=1 Z={26} n_species=1 species_Z={26}" > '+str(param['OUTPUT_PATH'])+'vectors.dat'
#
subprocess.call(cmd,shell=True)
print('DVs are given in '+str(param['OUTPUT_PATH'])+'vectors.dat file')
print()

# Reading descriptor vectors
print('Reading reference descriptor vector the atoms are compared with')
print('Reading reference DV for defect-free atom.')
ref_dfa = np.array([np.loadtxt(param['BCC_VEC'])])
print('Reading reference DV for self-interstitial atom.')
ref_sia = np.array([np.loadtxt(param['INT_VEC'])])
print('Reading reference DV for next-to-a-vacancy atom.')
ref_ntv = np.array([np.loadtxt(param['VAC_VEC'])])
print('Reading reference DV for type-a atom.')
ref_tpa = np.array([np.loadtxt(param['TYPA_VEC'])])

i = 0
arr = []
dist_diff_dfa = []
dist_diff_sia = []
dist_diff_ntv = []
dist_diff_tpa = []

# Cleaning quip output file
# Start reading DV at line 11
for i in range(11,11+num_atom):
     dv_1 = np.array(linecache.getline(str(param['OUTPUT_PATH'])+'vectors.dat',i)[7::].split())
     dv = dv_1.astype(np.float)
     arr.append(dv)

# Computing difference distance
print('Computing distance differences')
diff_dfa = arr-ref_dfa
diff_sia = arr-ref_sia
diff_ntv = arr-ref_ntv
diff_tpa = arr-ref_tpa

# Printing out results
ddiff = open(str(param['OUTPUT_PATH'])+'dist_difference.dat','w')
ddiff.write("# atom DFA  SIA  ANtV Type_a\n")
for i in range(0,num_atom):
    dist_diff_dfa.append(math.sqrt(sum(yi**2 for yi in diff_dfa[i,:])))
    dist_diff_sia.append(math.sqrt(sum(yi**2 for yi in diff_sia[i,:])))
    dist_diff_ntv.append(math.sqrt(sum(yi**2 for yi in diff_ntv[i,:])))
    dist_diff_tpa.append(math.sqrt(sum(yi**2 for yi in diff_tpa[i,:])))
    ddiff.write("{0:<3} {1:.10f} {2:.10f} {3:.10f} {4:.10f} \n".format(i+1, dist_diff_dfa[i], dist_diff_sia[i], dist_diff_ntv[i], dist_diff_tpa[i]))
ddiff.close()
print('Results are given in: '+str(param['OUTPUT_PATH'])+'dist_difference.dat')
print()

# Creating image with histogram
print('Creating histogram of the distance difference between DVs')
plt.hist(dist_diff_dfa,bins=1000,facecolor='white',edgecolor='black', linewidth=1.0, histtype='stepfilled', density=False)
plt.ylabel('Counts')
#Limits for the Y and X axes
plt.text(0.07,1e+3,r'DFA', fontsize=10)
plt.text(0.175,1e+3,r'Distorted', fontsize=10)
plt.text(0.175,0.2e+3,r'area', fontsize=10)
plt.text(0.275,1e+3,r'Frenkel pairs', fontsize=10)
plt.ylim(0.1,)
plt.xlim(0.0,)
plt.yscale('log', nonposy='clip')
plt.axvline(x=0.238, color='r')
plt.axvline(x=0.165, color='b',linestyle='--')
plt.annotate("", xy=(0.3, 1e+2), xytext=(0.2375, 1e+2),arrowprops=dict(arrowstyle="->"))
plt.savefig(str(param['OUTPUT_PATH'])+'DV_histogram.png', format='png', dpi=1000)
plt.close()

# Deleting support variables
del arr, dist_diff_dfa, dist_diff_sia, dist_diff_ntv, dist_diff_tpa
del ref_dfa, ref_sia, ref_ntv, ref_tpa
gc.collect()

# Moving idx file in output folder
cmd = 'mv '+str(param['QUIP_XYZ'])+'.idx '+str(param['OUTPUT_PATH'])
subprocess.call(cmd,shell=True)

print('Histogram saved in '+str(param['OUTPUT_PATH'])+'DV_histogram.png')
print()


##############################################
# Preparing input for kdtree and visualization
##############################################

print("Calculating available volume per atom using VORO++")

# Creating temporary input file for voro++
with open(str(param['QUIP_XYZ']), 'r') as data:
    sym = []
    ind = []
    x = []
    y = []
    z = []
    for i, line in enumerate(data):
        if (i != 0 and i!=1):
            p = line.split()
            sym.append(str(p[0]))
            x.append(float(p[1]))
            y.append(float(p[2]))
            z.append(float(p[3]))
            ind.append(int(p[4]))
voro = open(str(param['OUTPUT_PATH'])+'voro.xyz','w')
for i in range(0,len(ind)):
    voro.write('%10d %12.7f %12.7f %12.7f\n' % (ind[i], x[i], y[i], z[i]))
voro.close()

voro_xmin, voro_xmax = min(x), max(x)
voro_ymin, voro_ymax = min(y), max(y)
voro_zmin, voro_zmax = min(z), max(z)

# Calculating available volume per atom using voro++
cmd =str(param['VORO_PATH'])+' -o -px -py -pz '+str(voro_xmin)+' '+str(voro_xmax)+' '+str(voro_ymin)+' '+str(voro_ymax)+' '+str(voro_zmin)+' '+str(voro_zmax)+' '+str(param['OUTPUT_PATH'])+'voro.xyz'
subprocess.call(cmd,shell=True)

# Removing temporary input for voro++
cmd = 'rm '+str(param['OUTPUT_PATH'])+'voro.xyz'
subprocess.call(cmd,shell=True)

print("Available volume stored in "+str(param['OUTPUT_PATH'])+"voro.xyz.vol")
print()

# Extracting DV information and creating new input file for kdtree and VisIt
print("Preparing file with atom locations and DVs")
# Reading DVs from QUIP results
with open(str(param['OUTPUT_PATH'])+'dist_difference.dat', 'r') as data:
    bcc = []
    inter = []
    void = []
    type_a = []
    for i, line in enumerate(data):
        if (i != 0):
            p = line.split()
            bcc.append(float(p[1]))
            inter.append(float(p[2]))
            void.append(float(p[3]))
            type_a.append(float(p[4]))

# Reading VORO++ results
with open(str(param['OUTPUT_PATH'])+'voro.xyz.vol', 'r') as data:
    av_vol = []
    for line in data:
        p = line.split()
        av_vol.append(float(p[4]))

# Writing output file
p = str(param['QUIP_XYZ']).split("/")
f_name = p[len(p)-1].split(".")[0]+".DVs.xyz"
withDV = open(str(param['OUTPUT_PATH'])+f_name,'w')
withDV.write('%12d\n' % num_atom)
withDV.write(' id x y z bcc t-interstitial void(1) type_a av_vol\n')
for i in range(0,len(ind)):
    withDV.write('%s %16.7f %16.7f %16.7f %16.7f %16.7f %16.7f %16.7f %14.5f\n' % (sym[i], x[i], y[i], z[i], bcc[i], inter[i], void[i], type_a[i], av_vol[i]))
withDV.close()

# Deleting support variables
del sym, bcc, inter, void, type_a, av_vol
gc.collect()

print("Created output file "+str(param['OUTPUT_PATH'])+f_name)
print()


######################################################
# Calculation of the nearest atom location with kdtree
######################################################

print("Calculating the nearest atom location with KDTREE")
print()

# Injecting input xyz file location in kdtree parameter file
cmd = 'sed -i "2s|.*|\''+str(param['OUTPUT_PATH'])+f_name+'\'|" '+str(param['KDTREE_PAR'])
subprocess.call(cmd,shell=True)

# Running kdtree to produce fort.52 output file
cmd = 'sed -ri "8s|^(.{0})(.{7})|0 0 0 0|" '+str(param['KDTREE_PAR'])
subprocess.call(cmd,shell=True)
cmd = '(echo "\''+str(param['KDTREE_PAR'])+'\'") | '+str(param['KDTREE_PATH'])
subprocess.call(cmd,shell=True)

# Running kdtree to produce fort.60/sample_points_in_voids.dat output file
cmd = 'sed -ri "1s|^(.{0})(.{4})|'+str(param['KDTREE_THR'])+'|" '+str(param['KDTREE_PAR'])
subprocess.call(cmd,shell=True)
cmd = 'sed -ri "8s|^(.{0})(.{7})|1 0 0 0|" '+str(param['KDTREE_PAR'])
subprocess.call(cmd,shell=True)
cmd = '(echo "\''+str(param['KDTREE_PAR'])+'\'") | '+str(param['KDTREE_PATH'])
subprocess.call(cmd,shell=True)

# Moving output files to output directory
cmd = 'mv ./fort.* '+str(param['OUTPUT_PATH'])
subprocess.call(cmd,shell=True)
cmd = 'mv ./sample_points_in_voids.dat '+str(param['OUTPUT_PATH'])
subprocess.call(cmd,shell=True)

print()
print("Results are stored in "+str(param['OUTPUT_PATH'])+"fort.*")
print("                  and "+str(param['OUTPUT_PATH'])+"sample_points_in_voids.dat")
print()


#############################
# Creation of near_d VTK file
#############################

print("Creating mesh for the NN distance")
print()

# Conversion of sample_points_in_voids.dat into grid VTK for visualization
import tvtk.api as ta

# Reading kdtree parameters
with open(param['KDTREE_PAR'],'r') as parameter:
    for i, line in enumerate(parameter):
        if i == 3:
            # 4th line
            p = line.split()
            xmin = float(p[0])
            ymin = float(p[1])
            zmin = float(p[2])
        elif i == 4:
            # 5th line
            p = line.split()
            xmax = float(p[0])
            ymax = float(p[1])
            zmax = float(p[2])
        elif i == 6:
            # 7th line
            p = line.split()
            xstepi = int(p[0])
            ystepi = int(p[1])
            zstepi = int(p[2])

# Reading sample_points_in_voids.dat file
near_d= str(param['OUTPUT_PATH'])+'/sample_points_in_voids.dat'
with open(near_d, 'r') as data:
   ind = []
   x = []
   y = []
   z = []
   dist = []
   for line in data:
      p = line.split()
      ind.append(int(p[0]))
      x.append(float(p[1]))
      y.append(float(p[2]))
      z.append(float(p[3]))
      dist.append(float(p[4]))

# Printing number of lines
print("From the kdtree parameter file:")
print("xmin, xmax, xstep: %f, %f, %d" %(xmin, xmax, xstepi))
print("ymin, ymax, ystep: %f, %f, %d" %(ymin, ymax, ystepi))
print("zmin, zmax, zstep: %f, %f, %d" %(zmin, zmax, zstepi))
print()

# Box properties and interval: min and max already defined.
# Converting steps to imaginary:
xstep = complex(0, xstepi)
ystep = complex(0, ystepi)
zstep = complex(0, zstepi)

# Create grid
origin = (xmin,ymin,zmin)
spacing = ((xmax-xmin)/(xstep.imag-1),(ymax-ymin)/(ystep.imag-1),(zmax-zmin)/(zstep.imag-1))

x_vtk, y_vtk, z_vtk = np.mgrid[xmin:xmax:xstep, ymin:ymax:ystep, zmin:zmax:zstep]

# Creating the matrix for the values
dist_matrix = np.zeros((xstepi,ystepi,zstepi))

dist_ind=0
loop_ind=1
for i in range(0,xstepi):
   for j in range(0,ystepi):
      for k in range(0,zstepi):
         if (dist_ind<len(ind)):
             if (loop_ind==ind[dist_ind]):
                 dist_matrix[i][j][k]=dist[dist_ind]
                 dist_ind=dist_ind+1
         loop_ind=loop_ind+1

# transpose the grid to have the correct values
dist_matrix = dist_matrix.T.copy()

# write vtk file
out_grid = str(param['OUTPUT_PATH'])+'near_d.vtk'
sg = ta.tvtk.StructuredPoints(origin=origin,spacing=spacing,dimensions=x_vtk.shape)  # Creates the VTK structure
sg.point_data.scalars = dist_matrix.ravel()                                          # Required to go from a 3D numpy to a VTK array. Transforms 3D matrix into 1D array
sg.point_data.scalars.name = 'near_d'                                                # Variable name in the VTK
ta.write_data(sg, out_grid)                                                          # Saving the data

# Deleting support variables
del ind, dist, x, y, z
gc.collect()

# Output written
print("The values of the NN distance are stored in "+out_grid)
print()


##########################
# Visualization with VisIt
##########################

print("Visualizing results with VISIT")
print()

# Setting parameters in the visualization script
cmd = 'sed -i "33s|.*|input_s = \''+str(param['OUTPUT_PATH'])+f_name+'\' |" '+str(param['VISIT_SCRIPT'])
subprocess.call(cmd,shell=True)

cmd = 'sed -i "34s|.*|input_v = \''+out_grid+'\' |" '+str(param['VISIT_SCRIPT'])
subprocess.call(cmd,shell=True)

out_frames = str(param['OUTPUT_PATH'])+'visualization/'
try:
    os.makedirs(out_frames)
except FileExistsError:
    # directory already exists
    print("\nWarning: output/frames directory exists. Frames will be overwritten.")
    input("         Press Enter to continue.")
    print()
cmd = 'sed -i "35s|.*|out_dir = \''+out_frames+'\' |" '+str(param['VISIT_SCRIPT'])
subprocess.call(cmd,shell=True)

cmd = 'sed -i "36s|.*|out_name = \''+str(param['VIS_NAME'])+'\' |" '+str(param['VISIT_SCRIPT'])
subprocess.call(cmd,shell=True)

if (param['MOVIE_CREATION'] == "True"):
    cmd = 'sed -i "38s|.*|fading = True |" '+str(param['VISIT_SCRIPT'])
    subprocess.call(cmd,shell=True)
    cmd = 'sed -i "39s|.*|n_frames = 360 |" '+str(param['VISIT_SCRIPT'])
    subprocess.call(cmd,shell=True)
else:
    cmd = 'sed -i "38s|.*|fading = False |" '+str(param['VISIT_SCRIPT'])
    subprocess.call(cmd,shell=True)
    cmd = 'sed -i "39s|.*|n_frames = 1 |" '+str(param['VISIT_SCRIPT'])
    subprocess.call(cmd,shell=True)

# Running VisIt
cmd = str(param['VISIT_PATH'])+' -cli -nowin -s '+str(param['VISIT_SCRIPT'])
subprocess.call(cmd,shell=True)

# Removing VisIt's log file
current = os.path.dirname(os.path.realpath(__file__))
cmd = 'rm -f '+str(current)+'/visitlog.py'
subprocess.call(cmd,shell=True)


#############################
# Single frame/Movie Creation
#############################

if (param['MOVIE_CREATION'] == "True"):
    print("Creating movie")
    print()
    
    # Creating movie using bash script
    cmd = str(param['MOVIE_SCRIPT'])+' '+out_frames+' '+str(param['VIS_NAME'])
    subprocess.call(cmd,shell=True)
    
    print("Movie stored in "+str(param['OUTPUT_PATH'])+"visualization/movie/"+str(param['VIS_NAME'])+".mp4")
    print()
else:
    fileV    = out_frames+str(param['VIS_NAME'])+'-0000_V.png'
    fileR    = out_frames+str(param['VIS_NAME'])+'-0000_R.png'
    fileG    = out_frames+str(param['VIS_NAME'])+'-0000_G.png'
    fileB    = out_frames+str(param['VIS_NAME'])+'-0000_B.png'
    fileVR   = out_frames+str(param['VIS_NAME'])+'-0000_VR.png'
    fileVRG  = out_frames+str(param['VIS_NAME'])+'-0000_VRG.png'
    fileVRGB = out_frames+str(param['VIS_NAME'])+'-0000_VRGB.png'
    cmd = '(composite -compose add '+fileV+' '+fileR+' '+fileVR+' && composite -compose add '+fileVR+' '+fileG+' '+fileVRG+' && composite -compose add '+fileVRG+' '+fileB+' '+fileVRGB+') && rm -f '+fileVR+' '+fileVRG
    subprocess.call(cmd,shell=True)

    print("Single frame stored in "+str(param['OUTPUT_PATH'])+"visualization/"+str(param['VIS_NAME'])+"-0000_VRGB.png")
    print()


##########
# Epilogue
##########

print()
print(" ------------------------------------------------- ")
print("|     Fingerprint and visualization completed!    |")
print(" ------------------------------------------------- ")
print()
