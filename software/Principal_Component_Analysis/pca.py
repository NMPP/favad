#####################################################################################################
## Copyright (C) 2020 Udo von Toussaint, F. J. Dominguez-Gutierrez, Markus Rampp, Michele Compostella
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
#####################################################################################################
##
## General Notes:
## FaVaD: Fingerprint And VisuAlization of Defects. Version: 1.0
## This software provides a workflow for fingerprinting and visualizing defects in damaged crystal 
## structures. The theoretical background is presented in: paper_ID
## 
#####################################################################################################
## Warning:
## This is python script to apply the principal component analysis 
## to the descriptor vectors of unclassified atoms.
## It has to be done for a particular system, if it is needed.
##
######################################################################################################

##########
# Preamble
##########

import sys, argparse
import os, subprocess
import gc
import numpy as np
from pylab import *
from numpy import *
import linecache
import matplotlib.pyplot as plt
from sklearn.decomposition import TruncatedSVD

os.system('clear')
print()
print(" ------------------------------------------------- ")
print("|     Fingerprint and visualization of defects    |")
print("|          in damaged crystal structures          |")
print(" ------------------------------------------------- ")
print()

# Check Python version
if sys.version_info[0] < 3:
    print("\nRequires Python version 3 or greater\n")
    sys.exit()


#####################################################
## The following input parameters are set for the Fe case, 
## which is explaining in the paper
##########################################################

num_atom = 314928
i = 0
arr = []
dist_diff = []
# Cleaning quip output file
# Start reading atom DV at line 13
for i in range(13,13+num_atom):
     dv_1 = np.array(linecache.getline('vectors.dat',i)[7::].split())
     dv = dv_1.astype(np.float)
     arr.append(dv)


## Reading the id of the unclassified atoms in the damaged Fe sample
ref_v = np.loadtxt('id_atoms.dat')

## Extracting the DV of the unclassified atom from the set of atoms DVs.
Ar = []
for d in range(0,lent1):
    i = int(ref_v[d])
    Ar.append(arr[i])

Ar = np.array(Ar)

ffile = open('new_type.dat','w')
for i in range(0,50):
    ffile.write("{} {} \n".format(i,Ar[0,i])) 

#computing mean value of each component column
A_m = np.mean(Ar.T,axis=1)
Ap = Ar-A_m

## Appplying PCA by using
## Singular Value Decomposition
svd = TruncatedSVD(n_components=2)
svd.fit(Ap)
result = svd.transform(Ap)
thefile = open('pca_results.dat','w')
lengt = len(result[:,0])
print("lengt of the truncated vector: {0}".format(lengt))
for i in range(0,lengt):
    thefile.write("{0} {1} {2} \n".format(-result[i,0],' ', -result[i,1]))

## Plotting the results for a cluster analysis
fig, ax = plt.subplots()
rc('axes', linewidth=2)
plt.scatter(-result[:,0],-result[:,1],alpha=1.00,color="blue",marker="x",s=50,linewidth=2)
circl = plt.Circle((0.03,-0.001),0.055,color='black',fill=False,linewidth=2)
ax.add_artist(circl)
ax.xaxis.get_major_ticks()
plt.xlim(-0.4,0.3)
plt.ylim(-0.2,0.2)
plt.text(0.20,0.15,'a)',fontsize=35)
plt.xlabel('Principal Component 1 ', fontsize=12)
plt.ylabel('Principal Component 2 ', fontsize=12)
for axis in ['top','bottom','left','right']:
  ax.spines[axis].set_linewidth(1.5)

plt.savefig('pca_svd.png',format='png',dpi=1000)
