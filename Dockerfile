#####################################################################################################
## Copyright (C) 2020 Udo von Toussaint, F. J. Dominguez-Gutierrez, Markus Rampp, Michele Compostella
##              Max-Planck-Institut für Plasmaphysik, Boltzmannstrasse 2, 85748 Garching, Germany
##              Max-Planck Computing and Data Facility, Giessenbachstrasse 2, 85748 Garching, Germany
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
#####################################################################################################
##
## General Notes:
## Docker file for Fingerprinting and Visualizing defects in damaged crystal structures
## Version: 1.0
## This software provides a workflow for fingerprinting and visualizing defects in damaged crystal 
## structures. The theoretical background is presented in: paper_ID
##
#####################################################################################################

# Importing Ubuntu image
FROM ubuntu:18.04

# Updating distro
RUN \
  apt-get update && \
  apt-get upgrade -y

# Installing basic packages
RUN \
  apt-get install -y apt-utils unzip git cpio vim && \
  apt-get install -y gcc gfortran libblas-dev liblapack-dev python python-pip python3 python3-pip ffmpeg imagemagick && \
  apt-get install -y libsm6 libgl1 libgl1-mesa-dri libgl1-mesa-glx libpcre3-dev libxt6 && \
  apt-get autoclean && \
  apt-get autoremove

# Installing Python3 modules
RUN pip3 install numpy matplotlib vtk
RUN pip3 install mayavi

# Adding standard user
RUN adduser --disabled-password --gecos '' --uid 1000 --gid 100 favad

# Environment variables
ENV DEBIAN_FRONTEND noninteractive
ENV HOME /home/favad
ENV PREF $HOME/software
ENV QUIP_ARCH linux_x86_64_gfortran
ENV QUIP_STRUCTS_DIR $PREF/QUIP/structs
ENV QUIP_INSTALLDIR $PREF/QUIP/bin

# Copying software source files
COPY ./software /tmp/software
RUN chown -R favad /tmp/software

# Copying sample files
COPY ./sample $HOME/sample
RUN  chown -R favad $HOME/sample

# Switching to standard user
USER favad
WORKDIR $HOME

# Creating installation folder
RUN mkdir -p $PREF

# Installing QUIP
RUN \
  tar xvzf /tmp/software/QUIP.tar.gz -C $PREF/. && \
  cd $PREF/QUIP/ && \
  tar xvzf /tmp/software/GAP.tar.gz -C $PREF/QUIP/src/. && \
  (echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo y ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ; echo ) | make config && \
  make && \
  make install 

# Installing VORO++
RUN \
  mkdir -p $PREF/voro++ && \
  tar xvzf /tmp/software/voro++-0.4.6.tar.gz -C $PREF/voro++ --strip-components 1 && \
  cd $PREF/voro++/ && \
  sed -i "21s|.*|PREFIX=$PREF/voro++|" ./config.mk && \
  make && \
  make install

# Installing KDTREE2
RUN \
  tar xvzf /tmp/software/kdtree2.tar.gz -C $PREF/. && \
  cd $PREF/kdtree2/ && \
  gfortran -c kdtree2.f90 && \
  gfortran -o kdtree2_iaea_v03.x kdtree2_iaea_v03.f90 kdtree2.o

# Installing VisIt
RUN \
  mkdir -p $PREF/visit/ && \
  cd /tmp/software/ && \
  chmod 755 ./visit-install2_13_3 && \
  (echo 1) | bash ./visit-install2_13_3 2.13.3 linux-x86_64-ubuntu18-wmesa $PREF/visit/

# Copying script for PCA
RUN cp -r /tmp/software/Principal_Component_Analysis/ $PREF/.

# Removing installation packages
RUN rm -rf /tmp/software/

# Run bash when the container launches
CMD ["/bin/bash"]

